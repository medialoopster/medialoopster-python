# medialoopster

**medialoopster API wrapper for Python**

_medialoopster_ is a wrapper library for the [HTTP API](https://docs.medialoopster.com/api) of the
[medialoopster](https://www.medialoopster.com/) Media Asset Management System.


## Installation

```bash
pip install medialoopster
```
